package ScriereCod.Company;

import ScriereCod.Candidate.Candidate;
import ScriereCod.Department.Department;
import ScriereCod.Department.DepartmentName;
import ScriereCod.Department.Marketing;
import ScriereCod.Department.Production;
import ScriereCod.Exception.EvaluationIncapacityException;

import java.util.ArrayList;
import java.util.List;

public class Company {

    private String name;
    List <Candidate> candidates = new ArrayList<Candidate>();
    List <Department> departments = new ArrayList<Department>();

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public Company setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
        return this;
    }

    public Company() {

    }

    public Company(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Company setName(String name) {
        this.name = name;
        return this;
    }

    public void recruit() {
        Marketing marketing = new Marketing();
        Production production = new Production();
        for(Candidate c: candidates) {
            if(c.getDepartmentName().equals(DepartmentName.MARKETING)) {
                marketing.evaluate(c);
            } else if (c.getDepartmentName().equals(DepartmentName.PRODUCTION)){
                production.evaluate(c);
            } else {
                throw new EvaluationIncapacityException(c);
            }
        }
    }

    public void print(List c) {
        for(int i=0;i< c.size();i++) {
            System.out.println(c.get(i));
        }
    }
}
