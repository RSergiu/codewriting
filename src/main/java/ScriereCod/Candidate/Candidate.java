package ScriereCod.Candidate;

import ScriereCod.Department.DepartmentName;
import jdk.net.SocketFlow;

public class Candidate {

    private String name;
    private int levelOfCompetence;
    private DepartmentName departmentName;
    private CandidateStatus status;

    public Candidate() {

    }

    public Candidate(String name, int levelOfCompetence, DepartmentName departmentName) {
        this.name = name;
        this.levelOfCompetence = levelOfCompetence;
        this.departmentName = departmentName;
        this.status = CandidateStatus.AWAITING;
    }

    @Override
    public String toString() {
        return "Candidate name= " + name  + ", levelOfCompetence= " + levelOfCompetence + ", departmentName= " + departmentName + ", status=" + status;
    }

    public CandidateStatus getStatus() {
        return status;
    }

    public Candidate setStatus(CandidateStatus status) {
        this.status = status;
        return this;
    }


    public String getName() {
        return name;
    }

    public Candidate setName(String name) {
        this.name = name;
        return this;
    }

    public int getLevelOfCompetence() {
        return levelOfCompetence;
    }

    public Candidate setLevelOfCompetence(int levelOfCompetence) {
        this.levelOfCompetence = levelOfCompetence;
        return this;
    }

    public DepartmentName getDepartmentName() {
        return departmentName;
    }

    public Candidate setDepartmentName(DepartmentName departmentName) {
        this.departmentName = departmentName;
        return this;
    }

}
