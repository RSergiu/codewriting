package ScriereCod.Candidate;

public enum CandidateStatus {
    ACCEPTED,
    REJECTED,
    AWAITING,
}
