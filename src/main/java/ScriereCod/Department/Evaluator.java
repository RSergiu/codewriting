package ScriereCod.Department;

import ScriereCod.Candidate.Candidate;

public interface Evaluator {

    void evaluate(Candidate candidate);
}
