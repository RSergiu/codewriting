package ScriereCod.Department;

import ScriereCod.Candidate.Candidate;
import ScriereCod.Candidate.CandidateStatus;

public class Department implements Evaluator{

    protected int minLevelOfCompetence;
    protected DepartmentName dName;

    public Department() {

    }

    public Department(DepartmentName name, int minLevelOfCompetence) {
        this.minLevelOfCompetence = minLevelOfCompetence;
        this.dName = name;
    }

    public int getMinLevelOfCompetence() {
        return minLevelOfCompetence;
    }

    public Department setMinLevelOfCompetence(int minLevelOfCompetence) {
        this.minLevelOfCompetence = minLevelOfCompetence;
        return this;
    }

    public DepartmentName getdName() {
        return dName;
    }

    public Department setdName(DepartmentName dName) {
        this.dName = dName;
        return this;
    }

    public void evaluate(Candidate candidate) {
        if(candidate.getLevelOfCompetence() < minLevelOfCompetence) {
            candidate.setStatus(CandidateStatus.REJECTED);
        } else {
            candidate.setStatus(CandidateStatus.ACCEPTED);
        }
    }
}
