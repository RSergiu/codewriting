package ScriereCod.Exception;


import ScriereCod.Candidate.Candidate;

public class EvaluationIncapacityException extends RuntimeException {

    public EvaluationIncapacityException(Candidate candidate) {
        // calling Exception(String message) constructor
         super(candidate + " evaluation is not valid!");
    }
}
